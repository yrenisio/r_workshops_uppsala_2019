<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <title>Statistics with R</title>
    <meta charset="utf-8" />
    <meta name="author" content="Yann Renisio" />
    <link href="libs/remark-css/rutgers-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="uppsala.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Statistics with R
## Season 2, Part 1 (or Part 4, maybe?)
### Yann Renisio
### Uppsala U.
### 2019/05/20 (updated: 2019-11-13)

---


class: middle



# Previously, on R workshop

## **Part one**: What are R and Rstudio? Why using them?

## **Part two**: How to *transform* data as you want

## **Part three**: How to *show* your results as you want

---

class: inverse, center, middle

# "Let's take it easy today and do a full recap"*

.footnote[*Actually this going to be pretty hardcore, so hang on (new) people! I promise I will help you]
---

# **Part one**: What are R and Rstudio, and why you should you use them?

## R &amp; Rstudio

- R is a software/language designed for statistics. 
- Rstudio is an interface to use R, that enhances greatly the **coherence** and  **abilities** of R.

These two software are free (nothing to pay) and free (anybody can use it in any way, transform it, make it evolve, etc.)

The online community of R users is huge, eager to help and (surprisingly) nice with beginners.

The best, near perfect, complete initiation to R is *R for Data Science* by G. Grolemund and H. Wickham. And it is **free** and online. So, no excuse.



---
## Why should **you** use R and Rstudio?

- Because you should do statistics (You have access to data that sociologists from any other country are not even dreaming of).
- It is now the best way to exchange with other people doing statistics (in all fields), hence the best way to make big and fast progress, and benefit from others' progress.
- Because if you aim to publish again in serious journals, you soon won't be able to just say:

&gt; "Hey, here is my article. I received my data on SPSS format, I then opened the file with SPSS, did a few things, then I exported the resulting table into an xlsx format to do GDA with SPAD then I exported the coordinates as a xls file to plot it via excel then I took a screen capture and pasted them on word. Please feel free to reproduce my work!"

What R will give you the opportunity to say is:

&gt; "Hi, please find attached my article (in pdf, word, and html format). the folder also includes the raw data and R scripts that can be used for a total replication of this article. Best, :)"


---
Other reason: Stats are easy, fun, and useful. Say, you are invited among top-left-corner French people you want to impress, and are asked to bring wine. Tricky right? Well, use a freely available database that contains 150930 reviews of wine to pick a good one, whithout destroying your budget or loosing time! I recommend the ones below the red line.  


![](part_4_files/figure-html/unnamed-chunk-1-1.png)&lt;!-- --&gt;
---
# **Part two**: How to transform your data the way you want it

- Data manipulation is in my opinion at least as important as statistical knowledge. Because the better you get at manipulating data, the more likely you are to find out ideas, angles of analyse, that others, working on the same data, will not have. 
- R has a hero, Hadley Wickham, who made data manipulation (and many other things) super simple through one single set of complementary tools: the Tidyverse.
- Very few verbs will suffice for you to do 99% of what you do:
    - **read_csv/write_csv** to import/export your data
    - **filter:** to keep the rows you want
    - **select:** to keep/sort/rename the columns you want
    - **arrange:** to sort the rows in the order you want
    - **mutate:** to create new variables
    - **group_by:** to make operations within subgroups of rows
    - **summarise:** to reduce your data to each subgroups
    - **gather/spread** to pivot your variables to long/wide format
    - **fulljoin/bind_rows/bind_cols** to merge different tables
    - **ggplot()** to make the most beautiful plots in the world

But let's remind the **main principles** and the **two** most important **symbols** in R
---


## Basic principles

- What happens in R, happens through Rstudio, **you never have to use R by itself**.

- **Nothing can go really wrong with Rstudio**: you will never risk to corrupt your data, and this is great.

- Rstudio works within "**projects**", that means **anything you do takes place in one single folder in your computer**: you decide where, and how to name that folder **(don't use space when you name something, never, spaces_are_devils))**.

- You have two ways to interact with Rstudio:
    
    - **The console**: It is the place **where R tells you its reaction to your commands**. It can also be used to ask not important things to R (you write something, like 1+1 and then press [ENTER]).
    
    - **The source**: This is where you write **the real things** you are doing, and **that you will want to keep, in scripts**. A script is a simple text file, it's super light and super powerful, and you will love them, I assure you. To send a command from a script, pres [CTRL]+[ENTER]


---
### The assignment symbol: **&lt;-** 

It tells R: "I want you to create an **object**, to name it as I want, and to be what I want"

```
my_favorite_number &lt;- 23
```

&gt; means, "Dear R, I want you to create an object whose name is "my_favorite_number" and whose value is 23

from there, if you do that, you will now have an object called "my_favorite_number" that will appear in your "environment" window. You can now divide your favorite number by 12345, or whatever.

**Most of the times** (like 99.9 % of the times), your objects will be **tables** (we call them tables, dataframes, tibbles, ...), which is a rectangular thing, with **rows**, **columns** and then **cells** at the intersection of these rows and columns. You know, right, tables.

The shortcut for this symbol is [ALT]+[-], but you can also simply type [&lt;] and then [-]. You know... "&lt;-"
---
 ### The pipe symbol: **%&gt;%** 

This tells R "wait for it". To remember it, you should think: **"and then"**.

Suppose we have a table in our environment called starwars (we do), with individuals that we know the height (a numeric column) and gender (a character column).


```r
library(tidyverse)

tall_women_in_starwars &lt;- starwars %&gt;% 
  filter(height &gt; 200) %&gt;% 
  filter(gender == "female")
```
So here, I ask R:

&gt; What's up R! Please create a new object called "tall_women_in_starwars", which value is the "starwars" table, **and then** keep only those whose height is over 200 cm, **and then** keep only those who are of the female gender. Thx!

The shortcut for this symbol is [CTRL]+[SHIFT]+[M], but you can also simply type [%] and then [&gt;] and then [%]. You know... "%&gt;%"


---
## OK, so now let's get practical.

- Create a new project, choose a convenient place to store it in your computer, and call it "example_grades"

- Go there: https://archive.ics.uci.edu/ml/datasets/student+performance
- Click on the "Data Folder" link to download a folder.
- Extract the content of the folder
- rename the file called **"student-mat.csv"** into **"grades.csv"** 
- **Put this file in your project** (which means in the "example_grades" folder you've just created)

- Create a **new script**, save it and name it "example_grade".

**WHAT YOU'VE JUST DONE, ARE THE ONLY STEPS NECESSARY TO WORK WITH DATA IN R. YOU NEED A PROJECT, DATA, AND A SCRIPT**

---

- Now, in the script: 
    - load the tidyverse library
    - create an object, call it "grades" and tell R that the content of this object should be the content of the file you've just put in your folder. 
    
It means that it would be great if R could read this csv file. For that, the "read_csv()" function might be convenient... 

--


```r
library(tidyverse)

grades &lt;- read_csv(file = "grades.csv")
```

Did something appear in your environment window?

---

OK, now, for the sake of this exercise, we will only need six variables:
- sex,
- age,
- Medu (the level of education of the mother),
- Fedu (the level of education of the father),
- school (the name of the school),
- G3 (the final grade)

So, maybe you could **select** them?

--


```r
library(tidyverse)

grades &lt;- read_csv(file = "grades.csv")

grades &lt;- grades %&gt;% 
  select(sex, age, Medu, Fedu, school, G3)
```


---

Actually, I am a little intolerant: Not only I hate spaces, but also upper-case letters, and obscure variable names (Yes Statistics Sweden, I am talking about you). 

So could you please **rename** three ugly variables names so that:

- mother_education becomes the new name for Medu
- father_education becomes the new name for Fedu
- grade becomes the new name for G3

Thx!

--

So:


```r
grades &lt;- read_csv("grades.csv")

grades &lt;- grades %&gt;%
  select(sex, age, Medu, Fedu, school, G3)

grades &lt;- grades %&gt;%
  rename(mother_education = Medu, father_education = Fedu, grade = G3)

# Or

grades &lt;- read_csv("grades.csv") %&gt;%
  select(sex, age, Medu, Fedu, school, G3) %&gt;%
  rename(mother_education = Medu, father_education = Fedu, grade = G3)

# Or, even better actually (the "select" verb is awesome)

grades &lt;- read_csv("grades.csv") %&gt;%
  select(sex, age, mother_education = Medu,
         father_education = Fedu, school, grade = G3)
```
---

So now, we have a grades object that has six well named variables. Should we get to know a little about our data?

A first very basic thing... is to look at it, with the view() command


```r
view(grades)

# Or, to keep good habits:

grades %&gt;% view()
```

What about the number of males and females in the populations?

A simple way to do it, is to use the **count** function. As it is just exploratory, we can do this in the console, right?

--

```r
grades %&gt;% count(sex)
```

```
## # A tibble: 2 x 2
##   sex       n
##   &lt;chr&gt; &lt;int&gt;
## 1 F       208
## 2 M       187
```
We can do it for other variables too: what about the grade? The school? etc? If the result is a bit long, you can add the view() command to your "chain":


```r
grades %&gt;% count(grade) %&gt;% view
```
---
There is a basic function to know a bit more about your data: summary


```r
grades %&gt;% summary()
```

```
##      sex                 age       mother_education father_education
##  Length:395         Min.   :15.0   Min.   :0.000    Min.   :0.000   
##  Class :character   1st Qu.:16.0   1st Qu.:2.000    1st Qu.:2.000   
##  Mode  :character   Median :17.0   Median :3.000    Median :2.000   
##                     Mean   :16.7   Mean   :2.749    Mean   :2.522   
##                     3rd Qu.:18.0   3rd Qu.:4.000    3rd Qu.:3.000   
##                     Max.   :22.0   Max.   :4.000    Max.   :4.000   
##     school              grade      
##  Length:395         Min.   : 0.00  
##  Class :character   1st Qu.: 8.00  
##  Mode  :character   Median :11.00  
##                     Mean   :10.42  
##                     3rd Qu.:14.00  
##                     Max.   :20.00
```
---
But, if you want a better preview, I suggest the skimr package


```r
# install the package (just once in your "life")
# install.packages("skimr")
library(skimr) #Packages need to be loaded once by session
grades %&gt;% skim()
```

```
## Skim summary statistics
##  n obs: 395 
##  n variables: 6 
## 
## ── Variable type:character ───────────────────────────────────
##  variable missing complete   n min max empty n_unique
##    school       0      395 395   2   2     0        2
##       sex       0      395 395   1   1     0        2
## 
## ── Variable type:numeric ─────────────────────────────────────
##          variable missing complete   n  mean   sd p0 p25 p50 p75 p100
##               age       0      395 395 16.7  1.28 15  16  17  18   22
##  father_education       0      395 395  2.52 1.09  0   2   2   3    4
##             grade       0      395 395 10.42 4.58  0   8  11  14   20
##  mother_education       0      395 395  2.75 1.09  0   2   3   4    4
##      hist
##  ▆▇▇▆▂▁▁▁
##  ▁▆▁▇▁▇▁▇
##  ▂▁▂▇▆▆▂▁
##  ▁▃▁▆▁▆▁▇
```
---
We can also combine our "counts"


```r
grades %&gt;% count(sex, grade) %&gt;% view()
```

etc., but I guess now the time has come to be a bit more subtle...

Could we find the average grade by sex? To do so, we would need to create a new variable that would give us the **mean** of the grades, within each **group** of the sex variable.

A new variable is created via the **mutate()** function, the mean is computed with the **mean()** function, and the grouping of rows is handled by the *group_by()* function

So...

--

```r
grades %&gt;% group_by(sex) %&gt;% mutate(average_grade = mean(grade))
```

```
## # A tibble: 395 x 7
## # Groups:   sex [2]
##    sex     age mother_education father_education school grade average_grade
##    &lt;chr&gt; &lt;dbl&gt;            &lt;dbl&gt;            &lt;dbl&gt; &lt;chr&gt;  &lt;dbl&gt;         &lt;dbl&gt;
##  1 F        18                4                4 GP         6          9.97
##  2 F        17                1                1 GP         6          9.97
##  3 F        15                1                1 GP        10          9.97
##  4 F        15                4                2 GP        15          9.97
##  5 F        16                3                3 GP        10          9.97
##  6 M        16                4                3 GP        15         10.9 
##  7 M        16                2                2 GP        11         10.9 
##  8 F        17                4                4 GP         6          9.97
##  9 M        15                3                2 GP        19         10.9 
## 10 M        15                3                4 GP        15         10.9 
## # … with 385 more rows
```

---
But there is a lot of repetition here, so we might prefer to have the information **summarised** by group:



```r
grades %&gt;% group_by(sex) %&gt;% summarise(average_grade = mean(grade))
```
What about grouping by sex and school?


```r
grades %&gt;% 
  group_by(father_education,sex,school) %&gt;%
  summarise(average_grade = mean(grade))
```
Summarising is nice, but keeping the number of individuals by groups can be very important, just like the count function was doing it. Actually, the count function is a "wrapper" of the group_by() + summarise() + n() functions.

I mean:


```r
grades %&gt;% count(sex)

# Is exactly the same as

grades %&gt;% group_by(sex) %&gt;% summarise(n = n())
```
What count() does, is grouping by the variable(s) you choose and then summarising by creating a new variable called n, that counts the number of rows by groups.

---
Also median is nice to compare with the mean...


```r
grades %&gt;% 
  group_by(
    father_education,
    school
  ) %&gt;%
  summarise(
    average_grade = mean(grade),
    nb_of_cases_per_group = n(),
    median_grade = median(grade)
  )
```

etc., etc...

---
I said I was intolerant with the variable names, but this is also true with the variable values. If we go back to the webside, we can see that the level of education of parents, which in our dataset is coded as a numerical variable, is not really a linear variable:

Highest education:
- 0 - none,
- 1 - primary education (4th grade),
- 2 - 5th to 9th grade,
- 3 - secondary education 
- 4 - higher education 

Also, if we look at the distribution of these categories...


```
## Joining, by = "educ_level"
```

```
## # A tibble: 5 x 3
##   educ_level father mother
##        &lt;dbl&gt;  &lt;int&gt;  &lt;int&gt;
## 1          0      2      3
## 2          1     82     59
## 3          2    115    103
## 4          3    100     99
## 5          4     96    131
```
We see that there is very few people in the "none" category, and

---
So, it would be good to recode these variables with name like:
    - "1 Primary education or less" if this value is lesser or equal to 1,
    - "2 5th to 9th grade" if this value is equal to 2,
    - "3 Secondary education" if this value is equal to 3, and
    - "4 Higher education" if this value is equal to 4.

And, actually, to use a classical simplification, it could be nice to create a new variable, called max_education_parent that would keep the highest educational level between the two parents.

Here is how to do it with R.


```r
grades_recoded &lt;- grades %&gt;% 
  mutate(
    max_education_parent = pmax(# take the max value 
      father_education,         # of variables in the same row
      mother_education
    ),
    max_education_parent = case_when(# recode if... else...
      max_education_parent &lt;= 1 ~ "1 Primary education or less",
      max_education_parent &lt;= 2 ~ "2 5th to 9th grade",
      max_education_parent == 3 ~ "3 Secondary education",
      max_education_parent == 4 ~ "4 Higher education"
    )
  )  
```

---
Better right?


```r
grades_recoded %&gt;% count(max_education_parent)
```

```
## # A tibble: 4 x 2
##   max_education_parent            n
##   &lt;chr&gt;                       &lt;int&gt;
## 1 1 Primary education or less    39
## 2 2 5th to 9th grade             96
## 3 3 Secondary education         103
## 4 4 Higher education            157
```
---
And it seems to be sociologically useful!

![](part_4_files/figure-html/unnamed-chunk-20-1.png)&lt;!-- --&gt;
---

You see my point? This is a trivial example of how R can help you to a better understanding of your data. And then to transform it in order to get what you are looking for. Especially if you plot them!
---

So, to finish (and we will focus on that the next session), how to plot:

You will use the ggplot function and tell it:
- what is the **data** this plot will rely upon,
- what parts of this data for what parts of this plot (**aes**thetics)
- what kind of plot (**geom**etries)


```r
ggplot(# I want a plot!
  data = grades_recoded, # based on this data
  mapping = aes(# with variables from this data
    x     = grade, # this for the horizontal axis
    color = max_education_parent# this for the color
  ) 
)+
  geom_density()# And I want you to draw the density distribution

# Thx!!!
```

---


```r
library(ggrepel)
ggplot(# I want a plot!
  data = starwars %&gt;% filter(mass&lt;300), # based on this data
  mapping = aes(# with variables from this data
    x     = mass, # this for the horizontal axis
    y     = height,
    label = name,
    color = gender
  ) 
)+
  geom_text(# And I want you to draw points
  size = 3
  )+
  scale_x_log10()
```
---
class: inverse, center, middle

# "Questions please!"*



.footnote[*I know it was a lot / too much, so help me help you for a better session next time]
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
