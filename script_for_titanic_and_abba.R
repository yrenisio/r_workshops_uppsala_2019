###################################################################################################
################################ This is a script for fun #########################################
##################################################################################################

# You survived the three worshop steps, that's impressive.
# Now I need you to 2 install two things before to start (you only need to do it once)
# 1- the Factominer package. Paste this in your console: install.packages("FactoMineR")
# 2- the ggrepel package. Paste this in your console: install.packages("ggrepel")

# The first one is to make gda, the second if for automatically finding the best spot for labels.

# Done? Good. Let's go. From now one, you just have to load the different lines and see what's happenning


###################### The libraries I need #################

library(tidyverse)
library(FactoMineR)
library(ggrepel)

###################### Data import ###########################

my_titanic     <- read_csv("my_titanic.csv")

abba_sentiment <- read_csv("abba_sentiment.csv")


##################### Neaaaaaar, faaaaaaaar, whereeeeeever you aaaaare ######


# Did the passenger's class have influence on the chance of survival in the Titanic?

class_titanic <- my_titanic %>% 
  group_by(class, survived) %>% 
  summarise(nb_of_people = n()) %>% 
  group_by(class) %>% 
  mutate(pct_survival = 100*nb_of_people/sum(nb_of_people)) %>% 
  filter(survived == "yes")


ggplot(
  data = class_titanic,
  mapping = aes(x = class, y = pct_survival)
  ) +
  geom_col()+
  scale_y_continuous(name = "% of survival")+
  ggtitle(label = "First class passengers survived more",
          subtitle = "Poor Leonardo :(")


# Did the passenger's gender have influence on the chance of survival in the Titanic?


gender_titanic <- my_titanic %>% 
  group_by(sex, survived) %>% 
  summarise(nb_of_people = n()) %>% 
  group_by(sex) %>% 
  mutate(pct_survival = 100*nb_of_people/sum(nb_of_people)) %>% 
  filter(survived == "yes")


ggplot(
  data = gender_titanic,
  mapping = aes(x = sex, y = pct_survival)
) +
  geom_col()+
  scale_y_continuous(name = "% of survival")+
  ggtitle(label = "Women passengers survived more",
          subtitle = "Poor Leonardo, again... :(")

# Did the passenger's gender and class had cumulated influence on the chance of survival in the Titanic?


class_gender_titanic <- my_titanic %>% 
  group_by(class, sex, survived) %>% 
  summarise(nb_of_people = n()) %>% 
  group_by(class, sex) %>% 
  mutate(pct_survival = 100*nb_of_people/sum(nb_of_people)) %>% 
  filter(survived == "yes")


ggplot(
  data = class_gender_titanic,
  mapping = aes(x = class, y = pct_survival, fill = sex)
) +
  geom_col(position = "dodge")+
  scale_y_continuous(name = "% of survival")+
  ggtitle(label = "Being a women in 1st class was a good idea, less so in 3rd class",
          subtitle = "Lucky you, Kate...")


##################### The wiiiiinnner taaaaaakes it'aaaaaallll ######

# What are the principles of oppositions between Abba's songs, given their lyrics?

my_pca <- PCA(abba_sentiment %>% select(anger:trust))

pct_var <- my_pca$eig %>% 
  as_tibble() %>%
  mutate(axis = row_number()) %>% 
  select(axis, pct_var = `percentage of variance`) %>% 
  mutate(pct_var = round(pct_var, 2))

coordinates_variables <- my_pca$var$coord %>% as.data.frame() %>% 
  rownames_to_column(var = "var")


ggplot(coordinates_variables, aes(Dim.2, -Dim.1))+
  geom_hline(yintercept = 0)+
  geom_vline(xintercept = 0)+
  #  geom_segment(aes(xend = 0, yend = 0),lineend = "square")+
  ggrepel::geom_text_repel(aes(label = var), col = "red")+
  scale_x_continuous(
    name = paste0(
      "Surprise/Anticipation - <- Axis 2 (",
      pct_var$pct_var[2],
      "%) -> Surprise/Anticipation +"
    ),
    limits = c(-.9, .9)
  )+
  scale_y_continuous(
    name = paste0(
      "Anger/Sadness/fear <- Axis 1 (",
      pct_var$pct_var[1],
      "%) -> Joy/Trust/Positive"
    )
  )+
  ggtitle(
    "The feeling's space of ABBA's songs, A structural approach",
    subtitle = "Plane of variables"
  )





coordinates_individuals <- abba_sentiment %>% 
  select(song) %>% 
  bind_cols(
    my_pca$ind$coord %>% as_tibble() 
  )

abba_sentiment %>% 
  select(song) %>% 
  bind_cols(
    my_pca$ind$coord %>% as_tibble() 
  ) %>% 
  ggplot(., aes(Dim.2, -Dim.1))+
  geom_hline(yintercept = 0, col = "grey")+
  geom_vline(xintercept = 0, col = "grey")+
  geom_point()+
  ggrepel::geom_text_repel(aes(label = song), size = 2)+
  ggtitle(
    "The feeling's space of ABBA's songs, A structural approach",
    subtitle = "Plane of individuals"
  )+
  scale_x_continuous(paste0("Surprise/Anticipation - <- Axis 2 (", pct_var$pct_var[2], "%) -> Surprise/Anticipation +"))+
  scale_y_continuous(paste0("Anger/Sadness/fear <- Axis 1 (", pct_var$pct_var[1], "%) -> Joy/Trust/Positive"))+
  theme_minimal()+theme(axis.ticks = element_blank(),axis.line = element_blank(),axis.text = element_blank())


