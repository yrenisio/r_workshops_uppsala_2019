---
title: "Statistics with R"
subtitle: "Part 1"
author: "Yann Renisio"
institute: "Uppsala U."
date: "2019/05/20 (updated: `r Sys.Date()`)"
output:
  xaringan::moon_reader:
    css: [uppsala.css, rutgers-fonts]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      
---
class: middle

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
library(tidyverse)
attendants <- read_csv("attendants_workshop")
```

# General framework

## **Part one**: What are R and Rstudio? Why using them?

## **Part two**: How to *transform* data as you want

## **Part three**: How to *show* your results as you want

---
class: inverse, center, middle

# **Part one**: What are R and Rstudio, and why you should you use them?


---

## What is R ?

.pull-right[![](R_logo.svg.png)]


- R is a programming language created in the 1990's as an implementation of an older language (S).

- Two important specifics:
    - It was created **for doing statistics** (unlike other languages like python, who have more general purposes)
    - It is absolutely **free** (in the two senses of not lucrative and totally open sources)

It has become one of the most used statistical software for these two reasons, especially combined with...

---
## ...Rstudio

.pull-right[![](Rstudio_logo.png)]

- Rstudio is an interface for R, launched in 2011, which enhances greatly the **coherence** and  **abilities** of R. 

.footnote[Rstudio is also free and open source.]

This plurality of skills in one single place is a game changer in science for **reproductibility** and **replicability** matters.


---


## Why should you use R and Rstudio?

- Because you should do statistics (keep in mind that you have access to data that sociologists from any other country are not even dreaming of).
- Because it is now the best way to exchange with other people doing statistics (in all fields), hence the best way to make big and fast progress.
- Because if you aim to publish again in prestigious journals, you soon won't be able to just say:


> "Here are my results!"

or

> "I received my data on SPSS format, 
> so I opend them in SPSS (at least 99 $ a month), did a few things , 
> then I exported it into SPAD (what, you don't know spad?? - around 1000 euros a year-) and did things, 
> then I exported the outputs in excel or visual whatever (at least 100 euros a year) and did things, 
> then I "screen captured" my graphs and pasted them on word. 
> **Please feel free to reproduce my work! Though of course you can trust me, I never make mistakes!**"

---
## Example of transparent text

Bonjour!

My name is Yann (yann.renisio@gmail.com). There is `r attendants %>% filter(here_ws_1 == "yes") %>% nrow()` people in this workshop, among which `r attendants %>% filter(here_ws_1 == "yes", r_installed == "yes") %>% nrow()` have R and Rstudio installed in their computers, which means I will have to take a few minutes with `r ifelse(sum(attendants$r_installed == "no") == 0, "nobody", paste(attendants %>% filter(r_installed == "no") %>% select(name)))`.

There is also `r sum(attendants$uses_r == "yes")` "useRs", and I hope that by the end of this workshop, this number will climb to `r nrow(attendants)`

Please ask as many questions as you want, it will help everybody (especially me!)

```{r, echo=FALSE, out.height=200, out.width=200,fig.cap="Distribution of useRs and (yet) non-useRs in this room"}
ggplot(attendants %>% count(uses_r), aes(uses_r, n))+geom_col()+theme_bw()+scale_x_discrete("UseR")+scale_y_continuous("Frequency")+theme(axis.text = element_text(size = 30))
```


---

## What can be done with R? (a silly example)

![hey](anim_test.gif)
---
class: inverse, center, middle

## Enough talking Let's try!

---

- Create a new project in Rstudio, call it "r_workshop", and save it somewhere easy to access.
- create a new r script in this project, call it "r_workshop", and save it.

Now let's do stuff.

```{r, eval=FALSE}

# install tidyverse (need to do it only once)

install.packages("tidyverse")

# Load tidyverse (need to do it every time)

library(tidyverse)

# Let's have a look at a database

View(starwars)

# Let's select some columns of this database

my_example_database <- select(starwars, c(name, height, mass, gender))

# Let's have a look at a database

View(my_example_database)

# Let's export this example

write_csv(my_example_database, "here_is_my_example_database.csv")

```

---
 You can look at this link:
[hey](g_intra.html)

